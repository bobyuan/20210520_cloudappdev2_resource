package com.example.springmvc.counterwebapp.service;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.stereotype.Service;

/**
 * Implementation of the CounterService using RAM.
 *
 * @author bobyuan
 */
@Service
public class CounterServiceImpl implements CounterService {
    /** Use thread safe ConcurrentHashMap to hold the counters. */
    private Map<String, Long> counters = new ConcurrentHashMap<>();


    @Override
    public void reset(final String counterName) {
        setValue(counterName, 0L);
    }

    @Override
    public void setValue(final String counterName, final Long counterValue) {
        if (counterValue < 0) {
            throw new IllegalArgumentException("counterValue must >= 0");
        } else {
            Long val = counterValue;
            counters.put(counterName, val);
        }
    }

    @Override
    public Long increase(final String counterName) {
        Long valNew;
        Long val = counters.get(counterName);
        if (val != null) {
        	// will restart from 0 if reaching to the max.
            valNew = val >= Long.MAX_VALUE ? 0L : val + 1;
        } else {
            // new counter start from 0, increased to 1.
            valNew = 1L;
        }

        counters.put(counterName, valNew);
        return valNew;
    }

    @Override
    public Long getValue(final String counterName) {
        Long valResult;
        Long val = counters.get(counterName);
        if (val != null) {
            valResult = val;
        } else {
            // start from 0.
            valResult = 0L;
            counters.put(counterName, valResult);
        }

        return valResult;
    }
}
