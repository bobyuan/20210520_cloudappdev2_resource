package com.example.springmvc.counterwebapp.controller;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.springmvc.counterwebapp.service.CounterService;

@Controller
@RequestMapping(value = "/")
public class HomeController {
    private final static org.slf4j.Logger logger = LoggerFactory.getLogger(HomeController.class);
    public static final String COUNTER_NAME = "webcounter";

    private CounterService counterService;

    /**
     * Used by Spring to set the member variable "counterService".
     */
    @Autowired
    public void setCounterService(CounterService counterService) {
        this.counterService = counterService;
    }

    @GetMapping("favicon.ico")
    public String favicon() {
        return "forward:/static/img/favicon.ico.png";
    }

    @GetMapping("/")
    public String home(Model m) {
    	// Get the current date and time
        LocalDateTime currentTime = LocalDateTime.now();
        
        Long counterValue = counterService.increase(COUNTER_NAME);
        logger.info("Request handler home() get called: {}", counterValue);

        m.addAttribute("message", "Welcome!");
        m.addAttribute("date", currentTime.format(DateTimeFormatter.ISO_DATE_TIME));
        m.addAttribute("counter", counterValue);

        // Spring uses InternalResourceViewResolver and return back index.jsp
        return "index";
    }
}
