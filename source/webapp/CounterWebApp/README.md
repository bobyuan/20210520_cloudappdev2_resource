# README

This is a simple web counter application, the counter will increase by 1 when refreshed.

**CounterWebApp** is using Spring MVC + Spring Test + JUnit 5 + Logback.

It is developed on Windows with OpenJDK 8.



Here are some developer notes.

1. Create the web application.

   ```shell
   # create the new web application.
   mvn archetype:generate -DgroupId=com.example.springmvc.counterwebapp -DartifactId=CounterWebApp -DarchetypeArtifactId=maven-archetype-webapp -DinteractiveMode=false
   
   # navigate to web application folder.
   cd CounterWebApp
   
   # generate Eclipse web project files.
   mvn eclipse:eclipse -Dwtpversion=2.0
   ```

2. Import the project into eclipse ID, configure it to "Maven Project". Add and update the source files according to the document. It is now ready to run or debug in eclipse.


3. Or to test run using Maven.

   ```shell
   # clean up, compile and perform unit test.
   mvn clean compile test
   
   # launch in Jetty server.
   mvn jetty:run
   
   # or luanch in Tomcat server.
   mvn tomcat7:run
   ```
   
   Access the web application via link: http://localhost:8080/


4. You can also build release package (war).
   
   ```shell
   # build the release package.
   mvn package
   ```

5. Or generate static website with reports.
   
   ```shell
   # generate a site with reports.
   mvn site
   ```
   
   Open the "index.html" from "target/site" folder.

