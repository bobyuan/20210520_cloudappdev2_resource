<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page session="false"%>

<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <link rel="shortcut icon" type="image/x-icon" href="${pageContext.request.contextPath}/static/img/favicon.ico.png" />
  <title>CounterWebApp</title>
</head>

<body>
  <h1>CounterWebApp</h1>

  <img src="${pageContext.request.contextPath}/static/img/welcome.png" />
  <p>
    <strong>Message :</strong> ${message} <br/>
    <strong>Date : </strong> ${date} <br/>
    <strong>Counter :</strong> ${counter} <br/>
  </p>
</body>
</html>