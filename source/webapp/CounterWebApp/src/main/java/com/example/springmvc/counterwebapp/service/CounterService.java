package com.example.springmvc.counterwebapp.service;

/**
 * Service interface.
 *
 * @author bobyuan
 */
public interface CounterService {
    /**
     * Reset value of the counter to 0.
     * Note: It will create the counter if it does not exist.
     *
     * @param counterName The name of the counter.
     */
    void reset(final String counterName);

    /**
     * Set value of the counter.
     * Note: It will create the counter if it does not exist.
     *
     * @param counterName The name of the counter.
     * @param counterValue The value of the counter, it must be greater or equal to 0.
     */
    void setValue(final String counterName, final Long counterValue);

    /**
     * Increase the counter value by 1.
     * Note:
     *  - It will create the counter if it does not exist.
     *  - It will loop back to 0 if exceeded the maximum value (Long.MAX_VALUE).
     *
     * @param counterName The name of the counter.
     * @return The counter value after increase.
     */
    Long increase(final String counterName);

    /**
     * Get value of the counter.
     * Note: It will create the counter if it does not yet exist.
     *
     * @param counterName The name of the counter.
     * @return The value of the counter.
     */
    Long getValue(final String counterName);
}
