package com.example.springmvc.counterwebapp.controller;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.web.SpringJUnitWebConfig;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.example.springmvc.counterwebapp.service.CounterService;

/**
 * JUnit test of HomeController.
 *
 * @author bobyuan
 */
@Tag("junit5")
@ExtendWith(MockitoExtension.class)
@SpringJUnitWebConfig
@ContextConfiguration(locations = { "classpath:dispatcher-servlet.xml" })
public class HomeControllerTest {

    private MockMvc mockMvc;

    @InjectMocks
    private HomeController homeController;

    @Mock
    private CounterService counterService;

    /**
     * Setup test objects using Mock.
     */
    @BeforeEach
    public void beforeEach() {
        String counterName = HomeController.COUNTER_NAME;

        // Use mocked object to inject into HomeControler for testing.
        Mockito.lenient().when(counterService.increase(counterName)).thenReturn(124L);
        Mockito.lenient().when(counterService.getValue(counterName)).thenReturn(124L);
        homeController.setCounterService(counterService);

        // Setup Spring test in stand-alone mode.
        mockMvc = MockMvcBuilders.standaloneSetup(homeController).build();
    }

    @Test
    public void test_favicon() throws Exception {
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get("/favicon.ico").accept(MediaType.IMAGE_PNG);
        mockMvc.perform(builder).andExpect(status().isOk());
    }

    @Test
    public void test_home() throws Exception {
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get("/").accept(MediaType.TEXT_HTML);
        mockMvc.perform(builder).andExpect(status().isOk())
                // .andDo(MockMvcResultHandlers.print())
                .andExpect(view().name("index"))
                .andExpect(model().attribute("message", "Welcome!"))
                .andExpect(model().attributeExists("date"))
                .andExpect(model().attribute("counter", 124L));
    }

}
