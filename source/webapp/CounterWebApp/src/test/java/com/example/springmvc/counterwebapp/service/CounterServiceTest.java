package com.example.springmvc.counterwebapp.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.web.SpringJUnitWebConfig;

/**
 * JUnit test for CounterService.
 *
 * @author bobyuan
 */
@Tag("junit5")
@SpringJUnitWebConfig
@ContextConfiguration(locations = { "classpath:dispatcher-servlet.xml" })
public class CounterServiceTest {

    private static final String COUNTER_NAME_1 = "TestCounter1";
    private static final String COUNTER_NAME_2 = "TestCounter2";

    @Autowired
    private CounterService counterService;

    @Test
    @DisplayName("The auto-wired object should be available")
    public void testAutowiredObjects() {
        Assertions.assertNotNull(counterService);
    }

    @BeforeEach
    public void beforeEach() {
        counterService.reset(COUNTER_NAME_1);
        counterService.reset(COUNTER_NAME_2);
    }

    @Test
    public void test_reset_setValue() {
        // test reset().
        counterService.reset(COUNTER_NAME_1);
        Assertions.assertEquals(0L, counterService.getValue(COUNTER_NAME_1));

        // test setValue().
        counterService.setValue(COUNTER_NAME_1, 2L);
        Assertions.assertEquals(2L, counterService.getValue(COUNTER_NAME_1));

        counterService.setValue(COUNTER_NAME_1, 500L);
        Assertions.assertEquals(500L, counterService.getValue(COUNTER_NAME_1));

        // test reset().
        counterService.reset(COUNTER_NAME_1);
        Assertions.assertEquals(0L, counterService.getValue(COUNTER_NAME_1));
    }

    @Test
    public void test_setValue_invalid() {
        // valid cases.
        counterService.setValue(COUNTER_NAME_1, 0L);
        Assertions.assertEquals(0L, counterService.getValue(COUNTER_NAME_1));

        counterService.setValue(COUNTER_NAME_1, Long.MAX_VALUE);
        Assertions.assertEquals(Long.MAX_VALUE, counterService.getValue(COUNTER_NAME_1));

        // invalid case, will throw out IllegalArgumentException.
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            counterService.setValue(COUNTER_NAME_1, -1L);
        });
    }

    @Test
    public void test_increase() {
        counterService.reset(COUNTER_NAME_1);
        Assertions.assertEquals(0L, counterService.getValue(COUNTER_NAME_1));

        // normal cases.
        Assertions.assertEquals(1L, counterService.increase(COUNTER_NAME_1));
        Assertions.assertEquals(1L, counterService.getValue(COUNTER_NAME_1));

        Assertions.assertEquals(2L, counterService.increase(COUNTER_NAME_1));
        Assertions.assertEquals(2L, counterService.getValue(COUNTER_NAME_1));

        Assertions.assertEquals(3L, counterService.increase(COUNTER_NAME_1));
        Assertions.assertEquals(3L, counterService.getValue(COUNTER_NAME_1));

        // normal cases.
        counterService.reset(COUNTER_NAME_2);
        Assertions.assertEquals(0L, counterService.getValue(COUNTER_NAME_2));

        Assertions.assertEquals(1L, counterService.increase(COUNTER_NAME_2));
        Assertions.assertEquals(1L, counterService.getValue(COUNTER_NAME_2));

        Assertions.assertEquals(2L, counterService.increase(COUNTER_NAME_2));
        Assertions.assertEquals(2L, counterService.getValue(COUNTER_NAME_2));

        // exceptional cases #2.
        counterService.setValue(COUNTER_NAME_1, Long.MAX_VALUE);
        Assertions.assertEquals(Long.MAX_VALUE, counterService.getValue(COUNTER_NAME_1));

        Assertions.assertEquals(0L, counterService.increase(COUNTER_NAME_1)); // restart from 0 to avoid overflow.
        Assertions.assertEquals(0L, counterService.getValue(COUNTER_NAME_1));
    }

    @Test
    public void test_getValue() {
        // normal cases.
        counterService.reset(COUNTER_NAME_1);
        Assertions.assertEquals(0L, counterService.getValue(COUNTER_NAME_1));

        counterService.setValue(COUNTER_NAME_1, Long.MAX_VALUE);
        Assertions.assertEquals(Long.MAX_VALUE, counterService.getValue(COUNTER_NAME_1));

        // normal cases.
        counterService.reset(COUNTER_NAME_2);
        Assertions.assertEquals(0L, counterService.getValue(COUNTER_NAME_2));

        counterService.setValue(COUNTER_NAME_2, 500L);
        Assertions.assertEquals(500L, counterService.getValue(COUNTER_NAME_2));

        // not impact another counter.
        Assertions.assertEquals(Long.MAX_VALUE, counterService.getValue(COUNTER_NAME_1));
    }

    @Test
    public void test_basic_usage() {
        counterService.reset(COUNTER_NAME_1);
        Assertions.assertEquals(0L, counterService.getValue(COUNTER_NAME_1));

        Assertions.assertEquals(1L, counterService.increase(COUNTER_NAME_1));
        Assertions.assertEquals(1L, counterService.getValue(COUNTER_NAME_1));
        Assertions.assertEquals(2L, counterService.increase(COUNTER_NAME_1));
        Assertions.assertEquals(2L, counterService.getValue(COUNTER_NAME_1));
        Assertions.assertEquals(3L, counterService.increase(COUNTER_NAME_1));
        Assertions.assertEquals(3L, counterService.getValue(COUNTER_NAME_1));

        counterService.reset(COUNTER_NAME_2);
        Assertions.assertEquals(0L, counterService.getValue(COUNTER_NAME_2));

        counterService.increase(COUNTER_NAME_2);
        Assertions.assertEquals(1L, counterService.getValue(COUNTER_NAME_2));
        counterService.increase(COUNTER_NAME_2);
        Assertions.assertEquals(2L, counterService.getValue(COUNTER_NAME_2));

        // not impact another counter.
        Assertions.assertEquals(3L, counterService.getValue(COUNTER_NAME_1));
    }

    @Test
    public void test_extended_usage() {
        counterService.reset(COUNTER_NAME_1);
        Assertions.assertEquals(0L, counterService.getValue(COUNTER_NAME_1));
        counterService.reset(COUNTER_NAME_2);
        Assertions.assertEquals(0L, counterService.getValue(COUNTER_NAME_2));

        // counter1
        counterService.increase(COUNTER_NAME_1);
        Assertions.assertEquals(1L, counterService.getValue(COUNTER_NAME_1));
        counterService.increase(COUNTER_NAME_1);
        Assertions.assertEquals(2L, counterService.getValue(COUNTER_NAME_1));

        // counter2
        counterService.increase(COUNTER_NAME_2);
        Assertions.assertEquals(1L, counterService.getValue(COUNTER_NAME_2));
        counterService.increase(COUNTER_NAME_2);
        Assertions.assertEquals(2L, counterService.getValue(COUNTER_NAME_2));

        // counter2 set value.
        counterService.setValue(COUNTER_NAME_2, 5000L);
        Assertions.assertEquals(5000L, counterService.getValue(COUNTER_NAME_2));
        Assertions.assertEquals(5001L, counterService.increase(COUNTER_NAME_2));
        Assertions.assertEquals(5001L, counterService.getValue(COUNTER_NAME_2));

        // restart from 0 to avoid overflow.
        counterService.setValue(COUNTER_NAME_2, Long.MAX_VALUE);
        Assertions.assertEquals(Long.MAX_VALUE, counterService.getValue(COUNTER_NAME_2));
        Assertions.assertEquals(0L, counterService.increase(COUNTER_NAME_2));
        Assertions.assertEquals(0L, counterService.getValue(COUNTER_NAME_2));

        // counter1 reset.
        Assertions.assertEquals(3L, counterService.increase(COUNTER_NAME_1));
        Assertions.assertEquals(3L, counterService.getValue(COUNTER_NAME_1));
        counterService.reset(COUNTER_NAME_1);
        Assertions.assertEquals(0L, counterService.getValue(COUNTER_NAME_1));

        // counter2 continue.
        counterService.increase(COUNTER_NAME_2);
        Assertions.assertEquals(1L, counterService.getValue(COUNTER_NAME_2));
    }

}
